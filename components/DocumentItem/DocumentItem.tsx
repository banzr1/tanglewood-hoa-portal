import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemText from "@mui/material/ListItemText";
import Avatar from "@mui/material/Avatar";
import FolderIcon from "@mui/icons-material/Folder";
import Link from "@mui/material/Link";
import { ListItemButton } from "@mui/material";

export interface DocumentItemProps {
  fileName: string;
  filePath: string;
}

const DocumentItem = (props: DocumentItemProps) => {
  const { fileName, filePath } = props;

  return (
    <Link href={filePath} underline="none" color="inherit">
      <ListItemButton>
        <ListItemAvatar>
          <Avatar>
            <FolderIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary={fileName} />
      </ListItemButton>
    </Link>
  );
};

export default DocumentItem;
