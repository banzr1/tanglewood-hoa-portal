import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import * as tLogo from "../../public/big-t.png";
import Image from "next/image";
import { Button, Stack } from "@mui/material";
import Link from "next/link";

const Nav = () => {
  const [auth, _] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar position="static" sx={{ borderRadius: "1em", my: 2 }}>
      <Toolbar>
        <Stack direction="row" sx={{ flexGrow: 1 }}>
          <Link href="/">
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 1, ml: 0.2, width: "32px", height: "32px" }}
            >
              <Image src={tLogo} alt="LogoT" fill />
            </IconButton>
          </Link>
          <Typography variant="h6" component="div">
            Tanglewood HOA
          </Typography>
        </Stack>
        {auth ? (
          <Stack direction="row">
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              color="inherit"
              onClick={handleMenu}
            >
              <AccountCircle />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorEl)}
              anchorEl={anchorEl}
            >
              <MenuItem onClick={handleClose}>Profile</MenuItem>
              <MenuItem onClick={handleClose}>My account</MenuItem>
            </Menu>
          </Stack>
        ) : (
          <Stack direction="row" spacing={2}>
            <a
              target="_blank"
              rel="noreferrer"
              href="https://tanglewood-dues.cheddarup.com"
            >
              <Button variant="contained">Pay Dues</Button>
            </a>
          </Stack>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default Nav;
