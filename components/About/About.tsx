import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button } from "@mui/material";
import LocalLibraryIcon from "@mui/icons-material/LocalLibrary";
import Link from "next/link";

const About = () => {
  return (
    <Card sx={{ borderRadius: "1em", my: 2 }}>
      <CardMedia
        component="img"
        alt="Tanglewood trees"
        height="360"
        image="logo.webp"
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          Welcome to the Tanglewood Community
        </Typography>
        <Typography variant="body2" color="text.secondary">
          This trust is established for the perpetual preservation and the
          maintenance of the Open Areas, shown on the Plan and as set forth in
          the Maintenance Plan dated June 30, 1999 and filed with the Town of
          Middleborough (hereinafter the &quot;Trust Property&quot;) and is the
          organization of Lot Owners as required by the provisions of the Open
          Space and Resource Protection Development Bylaw of the Town under
          Section XVI in respect to Tanglewood, as an Open Space and Resource
          Protection Development as show in the Plan. All of the rights and
          powers in and with respect to the management and use of the Trust
          Property are conferred upon and exercisable by the Trustee and all
          property, real and personal, constituting or situated upon the Trust
          Property, are conveyed to the Trustees as Trustee of this Trust, IN
          TRUST to manage, administer and dispose of the same for the benefit of
          the Lot Owners from time to time of record.
        </Typography>
      </CardContent>
      <CardActions>
        <Link href="/learn">
          <Button startIcon={<LocalLibraryIcon />}>Documents</Button>
        </Link>
      </CardActions>
    </Card>
  );
};

export default About;
