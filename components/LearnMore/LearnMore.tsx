import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";

import List from "@mui/material/List";
import DocumentItem from "../DocumentItem";

const LearnMore = () => {
  function generate(element: React.ReactElement) {
    const files = [
      {
        name: "Declaration of Protective Covenants and Restrictions",
        path: "Declaration+of+Protective+Covenants+and+Restrictions.pdf",
      },
      { name: "Declaration of Trust", path: "Declaration+of+Trust.pdf" },
      { name: "Design Guidelines", path: "Design+Guidelines.pdf" },
      // {
      //   name: "Construction Design Submittal Template",
      //   path: "Construction-Design+Submittal+Sample+Template+Letter.pdf",
      // },
      {
        name: "Declaration of Reserved Easements",
        path: "Declaration+of+Reserved+Easements.pdf",
      },
      {
        name: "Restrictive Covenants-Driveway Lights",
        path: "Restrictive+Covenants-Driveway+Lights.pdf",
      },
      {
        name: "Town Planning Board Policy-Right of Way Landscaping",
        path: "Town+Planning+Board+Policy-Right+of+Way+Landscaping.pdf",
      },
    ];

    return files.map((file, index) =>
      React.cloneElement(element, {
        key: index,
        fileName: file.name,
        filePath: file.path,
      })
    );
  }

  return (
    <Card sx={{ borderRadius: "1em", my: 2 }}>
      <CardMedia
        component="img"
        alt="Tanglewood trees"
        height="300"
        image="trees.webp"
        sizes=""
      />
      <CardContent>
        <Typography gutterBottom variant="h5">
          Trust Documents
        </Typography>
        <List>{generate(<DocumentItem fileName="" filePath="/" />)}</List>
      </CardContent>
    </Card>
  );
};

export default LearnMore;
