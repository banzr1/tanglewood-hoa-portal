import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Button, Grid, IconButton, Link, Paper, Stack } from "@mui/material";
import ShareIcon from "@mui/icons-material/Share";
import { Box } from "@mui/system";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";

const ContactCard = () => {
  const copyAddress = () =>
    navigator.clipboard.writeText(
      "Tanglewood HOA PO Box 1253, Middleboro, MA 02346"
    );
  const copyEmail = () =>
    navigator.clipboard.writeText("tanglewoodhoatrust@gmail.com");

  return (
    <Card sx={{ borderRadius: "1em", my: 2 }}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          Contacts
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={6}>
            <Paper
              elevation={6}
              sx={{ p: 2, height: 110, borderRadius: "1em" }}
            >
              <Stack direction="row">
                <Box flexGrow={1}>
                  <Typography variant="overline">Mailing Address</Typography>
                  <br></br>
                  Tanglewood HOA PO Box 1253 <br></br>
                  Middleboro, MA 02346
                </Box>
                <Box justifyContent="center" alignItems="center" display="flex">
                  <IconButton onClick={copyAddress}>
                    <ContentCopyIcon />
                  </IconButton>
                </Box>
              </Stack>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12} md={6}>
            <Paper
              elevation={6}
              sx={{ p: 2, height: 110, borderRadius: "1em" }}
            >
              <Stack direction="row">
                <Box flexGrow={1}>
                  <Typography variant="overline">Email Address</Typography>
                  <br></br>
                  trustees@tanglewoodhoa.net
                </Box>
                <Box justifyContent="center" alignItems="center" display="flex">
                  <IconButton onClick={copyEmail}>
                    <ContentCopyIcon />
                  </IconButton>
                </Box>
              </Stack>
            </Paper>
          </Grid>
        </Grid>
      </CardContent>
      <CardActions>
        <Link href="Tanglewood Homeowners Association.vcf">
          <Button startIcon={<ShareIcon />}>Share</Button>
        </Link>
      </CardActions>
    </Card>
  );
};

export default ContactCard;
