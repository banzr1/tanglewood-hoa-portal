import { Box, Container } from "@mui/material";
import type { NextPage } from "next";
import Head from "next/head";
import LearnMore from "../components/LearnMore";
import Nav from "../components/Nav";

const Home: NextPage = () => {
  return (
    <Box>
      <Head>
        <title>Tanglewood HOA</title>
        <meta name="description" content="Tanglewood Homeowners Association" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container>
        <Nav />
        <LearnMore />

        <footer></footer>
      </Container>
    </Box>
  );
};

export default Home;
