import { Box, Container } from "@mui/material";
import type { NextPage } from "next";
import Head from "next/head";
import About from "../components/About";
import ContactCard from "../components/ContactCard";
import Nav from "../components/Nav";

const Home: NextPage = () => {
  return (
    <Box>
      <Head>
        <title>Tanglewood HOA</title>
        <meta name="description" content="Tanglewood HOA" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container>
        <Nav />
        <About />
        <ContactCard />

        <footer></footer>
      </Container>
    </Box>
  );
};

export default Home;
