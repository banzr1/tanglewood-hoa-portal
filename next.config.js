/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "prod-assets.cheddarcdn.com",
      },
    ],
    unoptimized: true,
  },
};

module.exports = nextConfig;
