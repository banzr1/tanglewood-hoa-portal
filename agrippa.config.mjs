// @ts-check
import { defineConfig } from "agrippa";

export default defineConfig({
  options: {
    typescript: true,
    baseDir: "components",
    componentOptions: {
      exportType: "default",
    },
  },
});
